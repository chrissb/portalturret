//
//  main.cpp
//  turretTest
//
//  Created by Chris Barr on 14/06/2014.
//  Copyright (c) 2014 chris. All rights reserved.
//

#include <iostream>
#include <string>
#include <time.h>
#include <unistd.h>
#include <stdio.h>
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/objdetect/objdetect.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include <vector>


using namespace std; // You forgot to use the std namespace
using namespace cv;

enum turret_state {SLEEPING=0, AWAKE=1, TRACKING=2, SHOOTING=3, DEAD=4};

//-------------INITIALISE VARIABLES----------//
int loop_sleep_time = 10000;
int state = SLEEPING;
int state_prev = state;

//leds and laser
int eye_led_brightness = 0;
int laser_brightness = 0;

//timers
clock_t t;
int wakeup_timer=-1;
int sleep_timer=-1;
string stateNames[5] = {"Sleeping","Awake","Tracking","Shooting","Dead"};

//sound effects
std::vector<std::string> sleepSounds = {"sleepmodeactivated.mp3","shuttingdown.mp3","goodnight.mp3","naptime.mp3","hibernating.mp3","resting.mp3"};
std::vector<std::string> wakeSounds = {"activated.mp3","hello.mp3"};
std::vector<std::string> seenYouSounds = {"targetacquired.mp3","iseeyou.mp3","thereyouare.mp3"};
std::vector<std::string> targetLostSounds = {"targetlost.mp3","searching.mp3","areyoustillthere.mp3"};


//face tracking
int face_tracked_for = 0;
int face_x = 0;
int face_y = 0;
int circ_width = 0;

//function headers
void playSound(string soundFile);    //Plays sound given filename
void informState();             //writes message if state changed
void updateTimers();            //update timers
void setTimer(int &timer,int min_seconds,int max_seconds);
void playFromGroup(vector<string> soundVec);
bool eyeBrightness(int led, int laser); //dims/brightens eye LED and Laser until they reach these values

int main(int argc, const char * argv[])
{
    
    
    srand (time(NULL)); //random time seed
    std::cout << "Start of main function\n";
    playSound("hello.mp3");
    
    
    
    //-----------------------FACE TRACKING SETUP------------------//
    //create the cascade classifier object used for the face detection
    cv::CascadeClassifier face_cascade;
    //use the haarcascade_frontalface_alt.xml library
    face_cascade.load("/Users/chris/Documents/code/turretTest/turretTest/haarcascade_frontalface_alt.xml");
    
    //setup video capture device and link it to the first capture device
    VideoCapture captureDevice;
    captureDevice.open(0);
    
    //setup image files used in the capture process
    Mat captureFrame;
    Mat grayscaleFrame;
    Mat sCaptureFrame;
    
    //create a window to present the results
    namedWindow("outputCapture", 1);
    //------------------------------------------------------------//
    
    
    //main loop
    while(1)
    {
        t = clock();
        
        //---------------------------------------------------------//
        //capture a new image frame
        captureDevice>>captureFrame;
        
        
        //webcam captured image is too big for fast face detection, resize
        resize(captureFrame,sCaptureFrame,Size(),0.25,0.25,CV_INTER_AREA);
        
        //convert resized image to gray scale and equalize
        cvtColor(sCaptureFrame, grayscaleFrame, CV_BGR2GRAY);
        equalizeHist(grayscaleFrame, grayscaleFrame);
        
        //create a vector array to store the face found
        std::vector<Rect> faces;
        
        //find faces and store them in the vector array
        face_cascade.detectMultiScale(grayscaleFrame, faces, 1.1, 3, CV_HAAR_FIND_BIGGEST_OBJECT|CV_HAAR_SCALE_IMAGE, Size(30,30));
        
        //draw a rectangle for all found faces in the vector array on the original image
        for(int i = 0; i < faces.size(); i++)
        {
            Point pt1(faces[i].x + faces[i].width, faces[i].y + faces[i].height);
            Point pt2(faces[i].x, faces[i].y);
            
            rectangle(sCaptureFrame, pt1, pt2, cvScalar(0, 255, 0, 0), 1, 8, 0);
        }
        
        if (faces.size()!=0)
        {
            face_x = faces[0].x + faces[0].width/2;
            face_y = faces[0].y + faces[0].height/2;
            circ_width = faces[0].width/2;
        }
        Point faceCenter(face_x,face_y);
        circle(sCaptureFrame,faceCenter,circ_width,cvScalar(0,0,255,0),1,8,0);
        
        //Keep a running tally of how long the face is tracked for
        if (faces.size()>=1 && face_tracked_for<200)
            face_tracked_for+=1;
        else if (face_tracked_for>0)
            face_tracked_for-=1;
        
        //print the output
        imshow("outputCapture", sCaptureFrame);
        //-------------------------------------------------------------//
        
        
        switch (state) {
            case SLEEPING:
            {
                
                //---------------WAKE TIMERS-----------------//
                //first, check if we're due to wake up
                if(wakeup_timer==0) //if countdown to waking up is finished
                {
                    state=AWAKE;    //wakeup
                    playFromGroup(wakeSounds);
                    wakeup_timer=-1;    //disable timer
                }
                else if (wakeup_timer == -1)    //if timer disabled
                {
                    setTimer(wakeup_timer,100,1000);   //set for random time, .5 -> 10 minutes
                    std::cout << "Waking in: " + to_string(wakeup_timer) + " loops\n";
                }
                //-------------------------------------------//
                if (face_tracked_for>100)
                {
                    state = AWAKE;
                    playFromGroup(wakeSounds);
                    face_tracked_for=0;
                }
                
                eyeBrightness(0, 0);
                

                break;
            }
            case AWAKE:
            {
                
                //---------------SLEEP TIMERS-----------------//
                //First, check if we're due to sleep
                if(sleep_timer==0) //if countdown to waking up is finished
                {
                    state=SLEEPING;    //wakeup
                    playFromGroup(sleepSounds);
                    sleep_timer=-1;    //disable timer
                }
                else if (sleep_timer == -1)    //if timer disabled
                {
                    setTimer(sleep_timer,50,100);   //set for random time
                    std::cout << "Sleeping in: " + to_string(sleep_timer) + " loops\n";
                }
                //-------------------------------------------//
                if (face_tracked_for>40)
                {
                    state=TRACKING;
                    playFromGroup(seenYouSounds);
                    face_tracked_for=5;
                }
                
                eyeBrightness(200, 200);

                break;
            }
            case TRACKING:
            {
                //std::cout << "Inside tracking case\n";
                eyeBrightness(255, 255);
                
                if (face_tracked_for==0)
                {
                    state=AWAKE;
                    playFromGroup(targetLostSounds);
                    
                }
                if (face_tracked_for>=40)
                    face_tracked_for=40;
                
                break;
            }
        }
        
        informState();
        state_prev = state;
        
        
        updateTimers();
        
        //t = clock()-t;
        usleep(loop_sleep_time);
        
    
    }
    
    return 0;
}



//plays sound given file name
void playSound(string fileName)
{
    std::cout << "Playing Sound: " + fileName + "\n";
    system(("afplay /Users/chris/Documents/code/PortalTurret/turretTest/sounds/"+fileName+" &").c_str());
}

//writes message whenever state changes
void informState()
{
    if (state!=state_prev)
        std::cout << "New state:" + stateNames[state] + "\n";
        
}

void updateTimers()
{
    if (wakeup_timer!=-1)
    {
        wakeup_timer-=1;
    }
    
    if (sleep_timer!=-1)
    {
        sleep_timer-=1;
    }

}

void setTimer(int &timer,int min_seconds,int max_seconds)
{
    timer = 100*(rand() % max_seconds-min_seconds + min_seconds);
}

void playFromGroup(vector<string> soundVec)
{
    int i = (rand() % soundVec.size());
    playSound((soundVec[i]));
}

bool eyeBrightness(int led, int laser)
{
    //----------------EYE and LASER--------------//
    //turn eye up to given brightness
    if (eye_led_brightness<led)
        eye_led_brightness+=1;
    if (eye_led_brightness>led)
        eye_led_brightness-=1;
    //turn laser up to given brightness
    if (laser_brightness<laser)
        laser_brightness+=1;
    if (laser_brightness>laser)
        laser_brightness-=1;
    //-------------------------------------------//
    if (eye_led_brightness==led && laser_brightness==laser)
        return true;
    else
        return false;
}